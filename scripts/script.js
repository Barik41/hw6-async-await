const btn = document.querySelector('.btn');
const container = document.querySelector('.container');

btn.addEventListener('click', async (e) => {
  try {
    const { data }  = await axios.get(`https://api.ipify.org/?format=json`);

    const dataIp = data.ip

    const ip  = await axios.get(`http://ip-api.com/json/${dataIp}`);

    const country = ip.data.country;
    const countryCode = ip.data.countryCode;
    const city = ip.data.city;
    const region = ip.data.regionName;
    const timezone = ip.data.timezone;

    container.insertAdjacentHTML('beforeend', `
      <p>Country: ${country}</p>
      <p>CountryCode: ${countryCode}</p>
      <p>City: ${city}</p>
      <p>Region: ${region}</p>
      <p>TimeZone: ${timezone}</p>
    `)


  } catch(err) {
    console.log(err)
  }
})


